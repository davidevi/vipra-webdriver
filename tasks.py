import os
from invoke.context import Context
from dotenv import load_dotenv

load_dotenv()

PYTEST_COMMAND_ARGS = "--cov=vipra --cov-report term-missing --cov-branch"
SRC_DIR = "src"
CONTEXT = Context()

def test():
    with CONTEXT.cd(SRC_DIR):
        os.environ["PYTHONPATH"] = "."
        CONTEXT.run(
            f"pytest {PYTEST_COMMAND_ARGS} tests",
            env=os.environ,
            pty=True,
            warn=True,
        )

# Runs a manual test
def manual():
    with CONTEXT.cd(SRC_DIR):
        os.environ["PYTHONPATH"] = "."
        CONTEXT.run(
            f"python tests/manual_test.py",
            env=os.environ,
            pty=True,
            warn=True,
        )

# Runs example1
run-ex1:
	cd docs/scripts && poetry run python example1.py

# Setup execution permissions and run setup
setup:
	chmod +x setup.bash && ./setup.bash
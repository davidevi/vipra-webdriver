#!/usr/bin/env bash
#      _   ___                 _      __    __      __    _             
#     | | / (_)__  _______ _  | | /| / /__ / /  ___/ /___(_)  _____ ____
#     | |/ / / _ \/ __/ _  /  | |/ |/ / -_) _ \/ _  / __/ / |/ / -_) __/
#     |___/_/ .__/_/  \_,_/   |__/|__/\__/_.__/\_,_/_/ /_/|___/\__/_/   
#         /_/ 
#
# Simple bash script that installs all the boilerplate code needed for the 
# Vipra Webdriver.
#
# Usage:
#   ./setup.sh <options>....
#
# Depends on:
#  .env file
#
# Github Repository: https://gitlab.com/vipra-tech/vipra-webdriver

###############################################################################
# Script Setup
###############################################################################

# Short form: set -u
# set -o nounset

# set -x
# Exit immediately if a pipeline returns non-zero.
# Short form: set -e
set -o errexit

# Print a helpful message if a pipeline with non-zero exit code causes the
# script to exit as described above.
trap 'echo "Aborting due to errexit on line $LINENO. Exit code: $?" >&2' ERR

# Allow the above trap be inherited by all functions in the script.
# Short form: set -E
set -o errtrace

# Return value of a pipeline is the value of the last (rightmost) command to
# exit with a non-zero status, or zero if all commands in the pipeline exit
# successfully.
set -o pipefail

# Set $IFS to only newline and tab.
IFS=$'\n\t'

###############################################################################
# Environment
###############################################################################

# $_ME
#
# This program's basename.
_ME="$(basename "${0}")"

###############################################################################
# Helpers
###############################################################################

# _print_logo()
#
# Usage:
#   _print_logo
#
# Description: 
#   Print the Vipra WebDriver Logo.
_print_logo() {
  cat <<HEREDOC
     _   ___                 _      __    __      __    _             
    | | / (_)__  _______ _  | | /| / /__ / /  ___/ /___(_)  _____ ____
    | |/ / / _ \/ __/ _  /  | |/ |/ / -_) _ \/ _  / __/ / |/ / -_) __/
    |___/_/ .__/_/  \_,_/   |__/|__/\__/_.__/\_,_/_/ /_/|___/\__/_/   
        /_/           

HEREDOC
}

# _print_help()
#
# Usage:
#   _print_help
#
# Description:
#   Print the program help information.
_print_help() {
  _print_logo
  cat <<HEREDOC
    
Simple bash script that installs all the boilerplate code needed for the 
Vipra Webdriver.

Usage:
  ${_ME} [<arguments>]
  ${_ME} -h | --help

Options:
  -h --help  Show this screen.
HEREDOC
}

###############################################################################
# Program Functions
###############################################################################

# _install_homebrew()
#
# Usage:
#   _install_homebrew
#
# Description:
#   Checks whether brew is installed or not, and if it is not, it installs it.
_install_homebrew() {
  echo '[Step 0] Installing Homebrew...'

  if command -v brew >/dev/null; then
    echo -e '\t\tHomebrew is already installed. Skipping this step.'
  else
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)" &&
    echo 'eval "$(/opt/homebrew/bin/brew shellenv)"' >> ~/.zprofile &&
    eval "$(/opt/homebrew/bin/brew shellenv)" &&
    echo -e '\t\tHomebrew is now installed'
  fi
}

# _install_git()
#
# Usage:
#   _install_git
#
# Description:
#   Checks whether git is installed or not, and if it is not, it installs it using homebrew.
_install_git() {
  echo '[Step 1] Installing Git...'

  if command -v git >/dev/null; then
    echo -e '\tGit is already installed. Skipping this step.'
  else
    brew install git &&
    echo -e '\t\Git is now installed'
  fi
}

# _clone_webdriver()
#
# Usage:
#   _clone_webdriver
#
# Description:
#   Asks for the Gitlab Username and Token and then clones the project.
_clone_selenium_journeys() {
  echo '[Step 2] Cloning the Selenium Journeys project...'

  git clone "https://gitlab+deploy-token-1757326:9-E_KsnZ46cG6nry3zh6@gitlab.com/vipra-tech/batch-manager/selenium-journeys.git" &&
  echo 'Downloaded the project successfully!'
}

# _install_python()
#
# Usage:
#   _install_python
#
# Description:
#   Install the correct version of Python for the Selenium Journeys project
_install_python() {
  echo '[Step 3] Installing Python 3.9...'

  brew install python@3.9 &&
  echo -e 'Python 3.9 is now installed.'
}

# _install_poetry()
#
# Usage:
#   _install_poetry
#
# Description:
#   Checks whether poetry is installed or not, and if it is not, it installs it using homebrew.
_install_poetry() {
  echo '[Step 4] Installing Poetry...'

  if command -v poetry >/dev/null; then
    echo -e '\tPoetry is already installed. Skipping this step.'
  else
    brew install poetry &&
    echo -e '\t\Poetry is now installed'
  fi
}

# _install_dependencies()
#
# Usage:
#   _install_dependencies
#
# Description:
#   Installs selenium and all the other requirements using Poetry
_install_dependencies() {
  echo '[Step 5] Installing Selenium Journeys Dependencies...'

  cd selenium-journeys
  poetry env use 3.9
  poetry install &&
  echo -e '\tAll Selenium Journeys dependencies are now installed'
}

# _setup_aliases()
#
# Usage:
#   _setup_aliases
#
# Description:
#   Adds a function called `journey` so that you can run the script like `journey example.py`
_setup_aliases() {
  echo '[Step 6] Setting up aliases...'

  cat <<HEREDOC >> ~/.zshrc
function journey ()
{
  poetry run python \$@
}
HEREDOC
}

# _install_vscode()
#
# Usage:
#   _install_vscode
#
# Description:
#   Checks whether VSCode is installed or not, and if it is not, it installs it using homebrew.
_install_vscode() {
  echo '[Step 7] Installing VSCode...'

  if brew list | grep -q 'visual-studio-code' || [ -d "/Applications/Visual Studio Code.app" ]; then
    echo -e '\tVSCode is already installed. Skipping this step.'
  else
    brew install --cask visual-studio-code &&
    echo -e '\t\VSCode is now installed'
  fi
}

# _open_vscode()
#
# Usage:
#   _open_vscode
#
# Description:
#   Opens VSCode in the current directory.
_open_vscode() {
  echo '[Step 8] Opening VSCode...'
  export PATH="$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
  code .
}

###############################################################################
# Main
###############################################################################

# _main()
#
# Usage:
#   _main [<options>] [<arguments>]
#
# Description:
#   Entry point for the program, handling basic option parsing.
_main() {
  # Avoid complex option parsing when only one program option is expected.
  if [[ "${1:-}" =~ ^-h|--help$  ]]
  then
    _print_help
  else
    _print_logo
    _install_homebrew
    _install_git
    _clone_selenium_journeys
    _install_python
    _install_poetry
    _install_dependencies
    _setup_aliases
    _install_vscode
    _open_vscode
  fi
}

# Call `_main` after everything has been defined.
_main "$@"
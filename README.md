# Vipra WebDriver

Wrapper around Selenium that provides more convenient methods, waiting, 
better exception handling, and logging.

See some example usage in [src/tests/manual_test.py](src/tests/manual_test.py).

The output is as follows: 

![Output](./docs/images/example_output.png)

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## 0.5.0 - 2021-08-25
- Fix bug when saving screenshots on errors
- Add method for implicity waiting

## 0.4.0 - 2021-08-25
- Allow users to provide their own wait timeouts
- Improve logging output
- Create own exception that is much clearer to read and see
- Get screenshots to also add a timestamp

## 0.3.0 - 2021-08-24
- Improve logging output
- Allow `get_element` to also retrieve by ID and class
- Disable retrieval of nested elements via `get_element`

## 0.2.0 - 2021-07-26

### Fixed
- Fix `WebDriver`.get_elements() accidentally including unwanted elements with a similar test-tag prefix. ([BM-567](https://vipra.atlassian.net/browse/BM-567))

## 0.1.0 - 2021-05-24

### Added
- Create `WebDriver` wrapper around selenium webdriver
- Create `WebElement` wrapper around selenium webdriver webelement
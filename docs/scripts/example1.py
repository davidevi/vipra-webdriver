import logging
import os

from dotenv import load_dotenv
from ddv.logging import log_to_stdout

from selenium.webdriver.common.keys import Keys
from vipra.webdriver import WebDriver

load_dotenv()

# BROWSER_BINARY_LOCATION = os.environ.get("browser_binary_location")
verbosity_filters = {1: ["vipra"], 2: ["selenium", "urllib3"]}

log_to_stdout(
    logging_level=logging.DEBUG,
    enable_colours=True,
    enable_indentation=True,
    verbosity_filters=verbosity_filters,
    verbosity_level=1,
    message_format="%(levelname)s  %(message)s",
)

wd = WebDriver(headless=False)

# ALWAYS COPY PASTE THE ABOVE IN EVERY FILE
###############################################################################

ENVIRONMENT = "https://wp3.nonprod.conbama.vipra.io"
wd.go_to(f"{ENVIRONMENT}/api/admin")

# Press the password button
wd.get_element(id="password-login").click()

# Login using username an password from .env file
USERNAME = os.environ.get("ADMIN_USERNAME")
PASSWORD = os.environ.get("ADMIN_PASSWORD")

wd.get_element(id="id_username").send_keys(USERNAME)
wd.get_element(id="id_password").send_keys(PASSWORD + Keys.ENTER)

# Go to Batches Page
wd.go_to(ENVIRONMENT)

# Edit a Bulk Batch
wd.get_element(id="list-group-bulk_batches-4").click(wait_page_change=True)

# If we had test-tags we could use those instead of this ugly XPath
EDIT_BUTTON_XPATH = '/html/body/div/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/button[2]'
wd.get_element(xpath=EDIT_BUTTON_XPATH).click()

BB_NUMBER_FIELD = '/html/body/div/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/div/div[2]/div[1]/div[1]/div/input'
wd.get_element(xpath=BB_NUMBER_FIELD).clear()
wd.get_element(xpath=BB_NUMBER_FIELD).send_keys('12345')

SAVE_BUTTON = '/html/body/div/div/div/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[2]/div/div[3]/div/div[2]/button'
wd.get_element(xpath=SAVE_BUTTON).click(wait_page_change=True)

# Check if editing worked
# BB_TITLE_XPATH = '/html/body/div/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/div/div[1]'
# bb_number = wd.get_element(xpath=BB_TITLE_XPATH).text

# assert bb_number == '12345'








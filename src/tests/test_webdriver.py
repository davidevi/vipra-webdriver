import os
from unittest import TestCase
from unittest import mock
from vipra.webdriver import WebDriver

BROWSER_BINARY_LOCATION = os.environ.get("BROWSER_BINARY_LOCATION")

mock_events = {}

class mock_selenium_webdriver(object):

    class ChromeOptions():
        binary_location = None

        def add_argument(arg1, arg2):
            mock_events["arg_added"] = True

    class Chrome:

        def __init__(self, options):
            mock_events["Chrome_called"] = True

        def get(self, arg):
            mock_events["get_called"] = True

        def quit(self):
            mock_events["quit_called"] = True

class TestWebDriver(TestCase):

    def setUp(self):
        mock_events = {}

    @mock.patch('vipra.webdriver.selenium_webdriver', mock_selenium_webdriver)
    def test_constructor_and_quit(self):
        wd = WebDriver(binary_location=BROWSER_BINARY_LOCATION)
        self.assertTrue(mock_events["Chrome_called"])
        wd.quit()
        self.assertTrue(mock_events["quit_called"])

    
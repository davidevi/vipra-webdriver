import os
import logging

from ddv.logging import log_to_stdout
from selenium.webdriver.common.keys import Keys

from vipra.webdriver import WebDriver

ENVIRONMENT = "http://davidevi.nonprod.conbama.vipra.io"
BROWSER_BINARY_LOCATION = os.environ.get("BROWSER_BINARY_LOCATION")
verbosity_filters = {1: ["vipra"], 2: ["selenium", "urllib3"]}

log_to_stdout(
    logging_level=logging.DEBUG,
    enable_colours=True,
    enable_indentation=True,
    verbosity_filters=verbosity_filters,
    verbosity_level=1,
    message_format="%(levelname)s  %(message)s",
)

wd = WebDriver(binary_location=BROWSER_BINARY_LOCATION)

wd.go_to(ENVIRONMENT)


wd.get_element(id="i0116").send_keys("incorrect" + Keys.ENTER)
wd.get_element(id="i0118").send_keys("incorrect" + Keys.ENTER)
wd._browser.implicitly_wait(10)

# wd.get_element('input-username').send_keys('test')
# wd.get_element('input-password').send_keys('test')
# wd.get_element('button-login').click(wait_page_change=True)
# wd.screenshot("logged-in")

wd.go_to(f"{ENVIRONMENT}/track_by_order")

packaged_batches = wd.get_elements("item-packaged-batch")

# list_packaged_batches = wd.get_element('list-packaged-batchess')
# list_packaged_batches.get_element('item-packaged-batch-0', wait=True).click()

# bapb = wd.get_element('button-assign-packaged-batches')
# bapb.click()

# checkbox = wd.get_element(
#     nested_tags=['button-assign-packaged-batches', 'check-assign-test'],
#     wait=False,
#     crash_if_not_found=False
# )

# other_element = wd.get_element('button-assign-packaged-batches')


# if not checkbox:
#     checkbox = wd.get_element(
#         nested_tags=['button-assign-packaged-batches', 'check-unassign-test'],
#         wait=False
#     )

# checkbox.click()
# bapb.click()


# wd.screenshot("assigned")
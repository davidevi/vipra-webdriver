import os
from datetime import datetime
import logging
import pathlib

from selenium import webdriver as selenium_webdriver
from selenium.webdriver.remote.webelement import WebElement as SeleniumWebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import (
    ElementNotVisibleException,
    NoSuchElementException,
    TimeoutException,
    ElementClickInterceptedException,
    StaleElementReferenceException,
)
from webdriver_manager.chrome import ChromeDriverManager

from vipra.exceptions import ElementNotFound

WAIT_TIMEOUT_SECONDS = 15
SCREENSHOT_PATH = "./out"
ERROR_SCREENSHOT_PATH = "./out/errors"
TEST_TAG_ATTRIBUTE = "test-tag"


class WebDriver(object):
    """
    Wrapper around the selenium web driver, with added functionality
    """

    logger = logging.getLogger(__name__ + ".WebDriver")

    def __init__(
        self,
        headless: bool = True,
        window_size: str = "1920,1080",
    ) -> "WebDriver":
        """
        Constructor; Creates the browser with all the settings
        """
        self.logger.debug("Browser initialising")

        chrome_options = selenium_webdriver.ChromeOptions()
        # self.logger.debug(f"Browser binary loaded from '{binary_location}'")
        # chrome_options.binary_location = binary_location
        chrome_options.add_argument("no-sandbox")

        if headless:
            self.logger.debug("Headless mode enabled")
            chrome_options.add_argument("headless")
        if window_size:
            self.logger.debug(f"Window size set to {window_size}")
            chrome_options.add_argument(f"window-size={window_size}")

        # self._browser = selenium_webdriver.Chrome(options=chrome_options)
        self._browser = selenium_webdriver.Chrome(executable_path=ChromeDriverManager().install(), options=chrome_options)
        self.logger.debug("Browser initialised")

        for path in [ERROR_SCREENSHOT_PATH, SCREENSHOT_PATH]:
            if not os.path.exists(path):
                os.mkdir(path)

    def get_elements(
        self,
        tag: str = None,
        wait: bool = True,
    ) -> list:

        if wait:
            self.logger.info(f"Waiting: '{tag}*'")
            WebDriverWait(
                driver=self._browser,
                timeout=WAIT_TIMEOUT_SECONDS,
                poll_frequency=0.25,
                ignored_exceptions=(ElementNotVisibleException),
            ).until(
                lambda x: len(
                    x.find_elements_by_xpath(
                        f"//*[starts-with(@{TEST_TAG_ATTRIBUTE}, '{tag}')]"
                    )
                )
                > 0
            )

        self.logger.info(f"Retrieving: '{tag}*'")
        raw_elements = self._browser.find_elements_by_xpath(
            f"//*[starts-with(@{TEST_TAG_ATTRIBUTE}, '{tag}')]"
        )

        def check_split_count(element):
            element_tag_difference = len(
                element.get_attribute("test-tag").split("-")
            ) - len(tag.split("-"))

            """
                if the item in question is a list-item, the tag_difference will be 1, as the index of
                the item is appended to the end of its test-tag, hence a difference of 1 is still valid
                
                example of a return False scenario:
                tag: "item-purchase-order"
                valid test-tag: "item-purchase-order-0" (tag_difference = 1)
                invalid test-tag: "item-purchase-order-item-0" (tag_difference = 2)
            """

            if (element_tag_difference) > 1:
                return False

            return True

        raw_elements[:] = [x for x in raw_elements if check_split_count(x)]

        elements = [
            WebElement(source_element=element, reference=tag, browser=self._browser)
            for element in raw_elements
        ]

        self.logger.info(f"Retrieved: {len(elements)} '{tag}*'")
        return elements

    def get_element(
        self,
        tag: str = None,
        id: str = None,
        cls: str = None,
        xpath: str = None,
        wait: bool = True,
        crash_if_not_found: bool = True,
        wait_timeout: int = None,
    ) -> "WebElement":
        """
        Given a test tag, retrieves an element that has the test tag
        """

        if tag:
            reference = f"[TAG:{tag}]"
            wait_condition = lambda x: x.find_element_by_css_selector(
                f"[{TEST_TAG_ATTRIBUTE}='{tag}']"
            ).is_displayed()
        if id:
            reference = f"[ID:{id}]"
            wait_condition = lambda x: x.find_element_by_id(id).is_displayed()
        if cls:
            reference = f"[CLS:{cls}]"
            wait_condition = lambda x: x.find_element_by_class_name(cls).is_displayed()

        if xpath:
            reference = f"[XPATH:{xpath}]"
            wait_condition = lambda x: x.find_element_by_xpath(xpath).is_displayed()

        if not tag and not id and not cls and not xpath:
            message = "Need to provide either a tag, an id, an xpath or a class"
            self.logger.error(message)
            raise Exception(message)

        try:
            if wait:
                if wait_timeout is None:
                    wait_timeout = WAIT_TIMEOUT_SECONDS
                self.logger.info(f"Waiting: {reference}")

                try:
                    WebDriverWait(
                        driver=self._browser,
                        timeout=wait_timeout,
                        poll_frequency=1,
                        ignored_exceptions=(ElementNotVisibleException),
                    ).until(wait_condition)
                except StaleElementReferenceException:
                    # Don't care, means it was found but it dissapeared
                    pass

            self.logger.info(f"Retrieving: {reference}")

            if tag:
                element = self._browser.find_element_by_css_selector(
                    f"[{TEST_TAG_ATTRIBUTE}='{tag}']"
                )
            if id:
                element = self._browser.find_element_by_id(id)
            if cls:
                element = self._browser.find_element_by_class_name(cls)
            if xpath:
                element = self._browser.find_element_by_xpath(xpath)
            

        except (NoSuchElementException, TimeoutException) as ex:
            message = f"Not Found: {reference}"
            self.logger.warning(message)
            if crash_if_not_found:
                self.screenshot(type(ex).__name__)
                raise ElementNotFound(message)
            else:
                return None

        # Wrap it in our own web element before going ahead
        return WebElement(
            source_element=element, reference=reference, browser=self._browser
        )

    def go_to(self, url: str, wait: bool = True, wait_timeout=None) -> None:
        """
        Loads the provided URL in the browser
        """
        old_url = self._browser.current_url
        self.logger.info(f"Going To: {url}")
        self._browser.get(url)

        if wait:
            if wait_timeout is None:
                wait_timeout = WAIT_TIMEOUT_SECONDS
            self.logger.info(f"Waiting: {url}")
            WebDriverWait(
                driver=self._browser,
                timeout=wait_timeout,
                poll_frequency=1,
            ).until(lambda x: old_url != x.current_url)

        self.logger.info(f"Current URL: {url}")

    def screenshot(self, file_name: str, prefix_time: bool = True) -> None:
        self._take_screenshot(SCREENSHOT_PATH, file_name, True)

    def _take_screenshot(
        self, prefix: str, file_name: str, prefix_time: bool = True
    ) -> None:
        time_prefix = datetime.now().strftime("%H%M%S-") if prefix_time else ""
        full_path = f"{prefix}/{time_prefix}{file_name}.png"
        absolute_path = (
            f"{pathlib.Path(full_path).parent.absolute()}/{time_prefix}{file_name}.png"
        )
        self.logger.info(f"Screenshot: {absolute_path}")
        self._browser.save_screenshot(full_path)

    def quit(self):
        """
        Closes the browser
        """
        self._browser.quit()
        self.logger.info("Browser exited")

    @property
    def current_url(self):
        self.logger.debug(f"Current URL: '{self._browser.current_url}'")
        return self._browser.current_url

    @property
    def title(self):
        self.logger.debug(f"Current Title: '{self._browser.title}'")
        return self._browser.title

    def wait(self, seconds):
        self._browser.implicitly_wait(seconds)


class WebElement(WebDriver):
    """
    Wrapper around the selenium web element, with added functionality
    """

    logger = logging.getLogger(__name__ + ".WebElement")

    def __init__(
        self,
        source_element: SeleniumWebElement,
        browser: selenium_webdriver.Chrome,
        reference: str = "",
    ) -> "WebElement":
        self.reference = reference if len(reference) > 0 else ""
        self.logger.debug(
            f"WebElement '{source_element.get_attribute(TEST_TAG_ATTRIBUTE)}' retrieved"
        )
        self._element = source_element
        # Overwriting browser so that get_element works _inside_
        # the element instead of all the elements
        self._browser = self._element
        self._real_browser = browser

    def send_keys(self, *args) -> None:
        self.logger.info(f"{self.reference} Keys: '{args}'")
        self._element.send_keys(*args)

    def click(self, wait_page_change: bool = False) -> None:
        self.logger.info(f"Clicking: {self.reference} ")
        old_url = self._real_browser.current_url
        try:
            self._element.click()
        except ElementClickInterceptedException:
            self.logger.info(f"Click failed, clicking parent: {self.reference} ")
            WebElement(
                source_element=self._element.find_element_by_xpath(".."),
                browser=self._browser,
            ).click()

        if wait_page_change:
            self.logger.info(f" Post-Click Waiting: {self.reference} {old_url}")
            try:
                WebDriverWait(
                    driver=self._real_browser,
                    timeout=WAIT_TIMEOUT_SECONDS,
                    poll_frequency=0.25,
                ).until(
                    lambda x: old_url != x.current_url
                    or not self._element.is_displayed()
                )
            except StaleElementReferenceException:
                # Good - Page did in fact reload
                pass
            self.logger.info(
                f" Post-Click new URL: {self.reference} {self._real_browser.current_url}"
            )

    @property
    def text(self):
        self.logger.debug(f"{self.reference} Text: '{self._element.text}'")
        return self._element.text

    def is_displayed(self):
        self.logger.debug(
            f"{self.reference} Is Diplayed: {self._element.is_displayed()}"
        )
        return self._element.is_displayed()

    def clear(self):
        self.logger.debug(f"Clearing text: {self.reference}")
        return self._element.clear()